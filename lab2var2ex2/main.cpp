#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>

using namespace std;

//DIVIDE ET IMPERA

typedef struct {
    int sx, sy, fx, fy;
} MATRIX_AREA;


void printMatrix(vector<vector<int>> matrix) {
    for(int i = 0; i < matrix.size(); i++) {
        for (int j = 0; j < matrix[i].size(); j++)
            cout << matrix[i][j] << ' ';

        cout << '\n';
    }

}

bool isPartOfMatrixEmpty(vector<vector<int>> matrix, int startX, int startY, int finalX, int finalY) {
    int sum = 0;
    for(int i = startY; i <= finalY; i++)
        for(int j = startX; j <= finalX; j++)
            sum += matrix[i][j];
    return sum == 0;
}

void fillMatrix(vector<vector<int>>& matrix, int& currentPiece, int startX, int startY, int finalX, int finalY) {
    if (startX >= finalX || startY >= finalY)
        return;
    if ((finalX - startX == 1) &&(finalY - startY == 1)) {
        //2 by 2 matrix
        //fill the positions and exit the recursion
        matrix[startX] = matrix[startX] == 1 ? 1 : currentPiece;
        matrix[startY] = matrix[startY] == 1 ? 1 : currentPiece;
        matrix[finalX] = matrix[finalX] == 1 ? 1 : currentPiece;
        matrix[finalY] = matrix[finalY] == 1 ? 1 : currentPiece;
        currentPiece++;
        return;
    }
    // divide the matrix into 4 smaller matrix
    // fill the 3 pieces from the center that are not filled so each quarter of the matrix has one filled piece
    MATRIX_AREA c1;
    c1.sx = startX;
    c1.sy = startY;
    c1.fx = startX + (finalX - startX - 1) / 2;
    c1.fy = startY + (finalY - startY - 1) / 2;

    if(isPartOfMatrixEmpty(matrix, c1.sx, c1.sy, c1.fx, c2.fy))
        fillMatrix(matrix, currentPiece, c1.sx, c1.sy, c1.fx, c1.fy);

    MATRIX_AREA c2;
    c2.sx = c1.sx;
    c2.sy = startY;
    c2.fx = startX + (finalX - startX - 1) / 2;
    c2.fy = startY + (finalY - startY - 1) / 2;

}


int main()
{

    //read n; 2^n x 2^n is the size of the matrix
    //read the position of the initial hashed element
    ifstream f("input.txt");
    int n, x, y;

    f >> n;
    f >> x >> y;        // o base positions

    int dim = pow(2, n);
    vector<vector<int>> matrix(dim, vector<int>(dim, 0));

    matrix[y][x] = -1;      //initial hash point
    int currentPiece = 1;

    cout << "Initial matrix: \n";
    printMatrix(matrix);



    return 0;
}
