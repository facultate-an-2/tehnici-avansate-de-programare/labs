#include <iostream>
#include <fstream>
#include <vector>

using namespace std;

/*
    input: n - nr activitati
    'nr' perechi (d,p) : -- (deadline, profit)
    Sortam descrescator dupa deadline desc
    Q coada de prioritati sortata descrescator dupa profit
    In momentul in care vreau sa planific o activ in slotul K(pana la timpul K)
    in Q vor fi toate activitatile ce pot candida pe acea pozitie si in varful cozii
    va fi candidatul optim
*/

bool descCompare(pair<int, int> i, pair<int, int> j) { return (i.first >= j.first); }

class CompareDist {
    public:
        bool operator()(pair<int, int> n1, pair<int, int> n2) {
            return n1.second>n2.second;
        }
}

int main()
{
    ifstream f("input.txt");
    vector<pair<int, int>> activities;
    vector<pair<int, int>> orderedActivies;

    int n, maxDeadline;
    f >> n >> maxDeadline;
    while(!f.eof()) {
        int deadline, profit;
        f>>deadline>>profit;
        activities.push_back(make_pair(deadline, profit));
        cout<<"read: " <<deadline << ", " << profit << '\n';
    }
    //sort the activities
    sort(activities.begin(), activities.end(), descCompare());

    //coada de prioritati
    priority_queue<pair<int, int>, vector<pair<int, int>>, CompareDist> q;



    return 0;
}
