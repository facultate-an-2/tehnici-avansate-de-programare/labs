#include <iostream>
#include <fstream>
#include <vector>
using namespace std;


/*	postordine = sdr
 *	preordine  = rsd
 *	inordine   = srd
 */
typedef struct t_node{
	int value;
	t_node *left, *right;
} node;

void printVector(vector<int> vec) {
	cout << "\n";
	for(vector<int>::iterator it = vec.begin(); it != vec.end(); ++it)
		cout << *it << ' ';
	cout << "\n";
}

vector<int> getSubvector(vector<int> initialVector, int startPosition, int endPosition) {
	vector<int>::const_iterator startIt = initialVector.begin() + startPosition;
	vector<int>::const_iterator endIt = initialVector.begin() + endPosition;
	return vector<int>(startIt, endIt);
}

void buildRsd(vector<int>& rsd, node *cNode) {
	if(cNode == NULL)
		return;
	rsd.push_back(cNode->value);
	buildRsd(rsd, cNode->left);
	buildRsd(rsd, cNode->right);
}

bool buildSubtree(vector<int> sdr, vector<int> srd, node *root, bool isLeftSubtree) {
	//check if the subtree is correct
	if(sdr.size() != srd.size()) {
		return false;
	}
	
	if(sdr.size() == 0)	//apelat degeaba
		return true;
	
	//get the current subtree root
	node *subroot = new node;
	subroot->value = sdr.back();
	subroot->left = NULL;
	subroot->right = NULL;
	if(isLeftSubtree)
		root->left = subroot;
	else
		root->right = subroot;
	
	if(sdr.size() == 1)
		return true;	//job done
	
	if(sdr.size() > 1) {
		//find the position of the subroot in the srd
		int srdRootPos = -1;
		for(int i = 0; i < srd.size(); i++)
			if(srd[i] == subroot->value)
				srdRootPos = i;
		if(srdRootPos == -1)
			return false;	//the subroot was not found in the srd so the subtree it's incorrect
		//call on left and right subsubtree
		vector<int> leftSrd = getSubvector(srd, 0, srdRootPos);
		vector<int> leftSdr = getSubvector(sdr, 0, leftSrd.size());
		
		vector<int> rightSrd = getSubvector(srd, srdRootPos + 1, srd.size());
		vector<int> rightSdr = getSubvector(sdr, leftSdr.size(), sdr.size() - 1);
		
		return buildSubtree(leftSdr, leftSrd, subroot, true) && buildSubtree(rightSdr, rightSrd, subroot, false);
	}
}
 
int main()
{
	if(!ifstream("date.in")) {
		cout << "Input file not found!\n";
		return 0;
	}
	
	ifstream f("date.in");
	ofstream g("date.out");
	
	int nodesCount, x;
	f >> nodesCount;
	
	vector<int> sdr, srd;
	for(int i = 0; i < nodesCount; i++) {
		f >> x;
		sdr.push_back(x);
	}
	for(int i = 0; i < nodesCount; i++) {
		f >> x;
		srd.push_back(x);
	}
	//build a fake root
	node *fakeRoot = new node;
	fakeRoot->value = -1;
	fakeRoot->left = NULL;
	fakeRoot->right = NULL;
	
	if(buildSubtree(sdr, srd, fakeRoot, true))
		cout << "Este arbore!\n";
	else
		cout << "Arbore incorect!\n";
		
	node *trueRoot = new node;
	trueRoot = fakeRoot->left;
	vector<int>rsd;
	buildRsd(rsd, trueRoot);
	printVector(rsd);
	printVector(srd);
	printVector(sdr);
	return 0;
}
