#include <iostream>
#include <fstream>
#include <vector>
using namespace std;

int findFixedPoint(vector<int> numbers, int startPosition, int endPosition) {

    if(startPosition > endPosition)
        return -1;
    if(startPosition == endPosition && numbers[startPosition] == startPosition)
        return startPosition;
    if(numbers[startPosition] == startPosition)
        return startPosition;
    if(numbers[endPosition] == endPosition)
        return endPosition;

    int middle = (startPosition + endPosition) / 2;
    if(numbers[middle] == middle)
        return middle;

    if(numbers[middle] > middle)
        return findFixedPoint(numbers, startPosition, middle - 1);
    else
        return findFixedPoint(numbers, middle + 1, endPosition);
}

///Cautare punct fix, i.e. daca intr-un vector ordonat crescator exista un element a.i. v[i] == i
int main()
{
	if(!ifstream("date.in")) {
		cout << "Input file not found\n";
		return 0;
	}
	
    ifstream f("date.in");
    ofstream g("date.out");
	
	vector<int> input;
	
    int n, x;
    f >> n;
	cout << "Read n: " << n << '\n';
	
    for (int i = 0; i < n; i++) {
		f >> x;
		input.push_back(x);
    }
	
	cout << "Elements: \n";
	for (int i = 0; i < n; i++)
		cout << input[i] << ' ';

    int pos = findFixedPoint(input, 0, input.size() - 1);
    if(pos != -1) {
		g << pos;
		cout << "The point was found at: " << pos << '\n';
	}
    else {
		cout << "The point was not found!\n";
		g << "nu exista";
	}
	
	f.close();
	g.close();
    return 0;
}
