#include <iostream>
#include <fstream>
#include <vector>
using namespace std;

int findFixedPoint(vector<int> numbers, int startPosition, int endPosition) {

    if(startPosition > endPosition)
        return -1;
    if(startPosition == endPosition && numbers[startPosition] == startPosition)
        return startPosition;
    if(numbers[startPosition] == startPosition)
        return startPosition;
    if(numbers[endPosition] == endPosition)
        return endPosition;

    int middle = (startPosition + endPosition) / 2;
    if(numbers[middle] == middle)
        return middle;

    if(numbers[middle] > middle)
        return findFixedPoint(numbers, startPosition, middle - 1);
    else
        return findFixedPoint(numbers, middle + 1, endPosition);
}

///Cautare punct fix, i.e. daca intr-un vector ordonat crescator exista un element a.i. v[i] == i
int main()
{
    ifstream f("date.in");
    ofstream g("date.out");
    vector<int> input;
    int n, x;
    f >> n;
    for (int i = 0; i < n; i++) {
        f >> x;
        if(x >= 0)
            input.push_back(x);
    }

    int pos = findFixedPoint(input, 0, input.size() - 1);
    if(pos != -1)
        g << pos;
    else
        g << "nu exista";

    return 0;
}
