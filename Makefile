.PHONY: clean All

All:
	@echo "----------Building project:[ NrMinimOperatiiInmultiriMatrici - Debug ]----------"
	@cd "NrMinimOperatiiInmultiriMatrici" && "$(MAKE)" -f  "NrMinimOperatiiInmultiriMatrici.mk"
clean:
	@echo "----------Cleaning project:[ NrMinimOperatiiInmultiriMatrici - Debug ]----------"
	@cd "NrMinimOperatiiInmultiriMatrici" && "$(MAKE)" -f  "NrMinimOperatiiInmultiriMatrici.mk" clean
