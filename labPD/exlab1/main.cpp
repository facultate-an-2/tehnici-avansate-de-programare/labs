#include <iostream>
#include <fstream>
/**
 * @brief Laborator 4, exercitiul 1
 * Se da o tabla de n x n populata cu numere strict pozitive. Un robotel pleaca din 1, 1. Determinati traseul cu punctaj maxim(optim).
 * Numerele fiind strict pozitive, evident traseul optim trebuie sa porneasca din coltul din stanga sus si sa ajunga in coltul din dreapta jos.
 * Folosim o matrice ajutatoare T, T[i][j] reprezentand punctajul maxim care poate fi obtinut plecand din (i, j). Matricea T trebuie calculata din coltul 
 * dreapta jos catre coltul stanga sus
 * @return 
 */
using namespace std; 
 
int main()
{
	ifstream f("date.in");
	if(!f) {
		cout << "date.in not found!";
		return 0;
	}
	int n;
	f >> n;
	int **M;
	int **T;
	M = new int*[n];
	for(int i = 0; i < n; i++)
		M[i] = new int[n];
	for(int i = 0; i < n; i++)
		for(int j = 0; j < n; j++) 
			f >> M[i][j];
	f.close();
	
	T = new int*[n];
	for(int i = 0; i < n; i++)
		T[i] = new int[n];
	
	for(int i = 0; i < n; i++)
		for(int j = 0; j < n; j++)
			T[i][j] = 0;
			
	//calculam T
	for(int i = n-1; i >= 0; i--)
		for(int j = n-1; j >= 0; j--)
			T[i][j] = M[i][j] + max((i == n-1 ? 0 : T[i+1][j]), (j == n-1 ? 0 : T[i][j+1]));
			
	int i = 0, j = 0;
	while(i < n - 1 && j < n - 1) 
	{
		if(T[i + 1][j] > T[i][j + 1]) {
			cout << "DOWN\n";
			i++;
		}
		else {
			cout << "RIGHT\n";
			j++;
		}
	}
	while(i++ != n - 1)
		cout << "DOWN\n";
	while(j++ != n - 1)
		cout << "RIGHT\n";
	return 0;
}
