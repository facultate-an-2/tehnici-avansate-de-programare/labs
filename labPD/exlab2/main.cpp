#include <iostream>
#include <fstream>
#include <cmath>
/**
 * @brief Laborator 4, exercitiul 2
 * Acelasi enunt, doar ca acum putem pleca de oriunde de pe prima linie(trebuie sa aflam de unde) si tabla poate contine si valori negative.
 * Tot trebuie sa ajunga in coltul din dreapta jos!
 * @return 
 */
using namespace std; 
 
int main()
{
	const float NEGATIVE_INFINITY = -1 * INFINITY;
	
	ifstream f("date.in");
	if(!f) {
		cout << "date.in not found!";
		return 0;
	}
	int n;
	f >> n;
	int **M;
	int **T;
	M = new int*[n];
	for(int i = 0; i < n; i++)
		M[i] = new int[n];
	for(int i = 0; i < n; i++)
		for(int j = 0; j < n; j++) 
			f >> M[i][j];
	f.close();
	
	T = new int*[n];
	for(int i = 0; i < n; i++)
		T[i] = new int[n];
	
	for(int i = 0; i < n; i++)
		for(int j = 0; j < n; j++)
			T[i][j] = 0;
	
	//calculam T
	for(int i = n-1; i >= 0; i--)
		for(int j = n-1; j >= 0; j--) { 
			if(i == n - 1 && j == n - 1) {
				T[i][j] = M[i][j];
				continue;
			}
			T[i][j] = M[i][j] + max((i == n-1 ? NEGATIVE_INFINITY : T[i+1][j]), (j == n-1 ? NEGATIVE_INFINITY : T[i][j+1]));
		}
			
	cout << "MATRICEA T\n";
	for(int i = 0; i < n; i++) {
		for(int j = 0; j < n; j++)
			cout << T[i][j] << ' ';
		cout << '\n';
	}
	
	int startI = 0, startJ = 0;
	
	//determinam punctul de start
	int max = NEGATIVE_INFINITY;
	for(int j = 0; j < n; j++)
		if(T[0][j] > max) {
			max = T[0][j];
			startJ = j;
		}
	int i = startI;
	int j = startJ;
	
	cout << "PUNCT START: (" << i << ", " << j << ")\n";
	while(i < n - 1 && j < n - 1) 
	{
		if(T[i + 1][j] > T[i][j + 1]) {
			cout << "DOWN\n";
			i++;
		}
		else {
			cout << "RIGHT\n";
			j++;
		}
	}
	
	while(i++ != n - 1)
		cout << "DOWN\n";
	while(j++ != n - 1)
		cout << "RIGHT\n";
	return 0;
}
