#include <iostream>
#include <fstream>
/**
 * @brief Laborator 4, exercitiul 1
 * Se da o tabla de n x n populata cu numere strict pozitive. Un robotel pleaca din 1, 1. Determinati traseul cu punctaj maxim(optim).
 * Numerele fiind strict pozitive, evident traseul optim trebuie sa porneasca din coltul din stanga sus si sa ajunga in coltul din dreapta jos.
 * Folosim o matrice ajutatoare T, T[i][j] reprezentand punctajul maxim care poate fi obtinut plecand din (i, j). Matricea T trebuie calculata din coltul
 * dreapta jos catre coltul stanga sus
 * @return
 */
using namespace std;

int main(int argc, char *argv[])
{
	ifstream f("date.in");
	if(!f) {
		cout << "date.in not found!";
		return 0;
	}
	int n;
	f >> n;
	int **M;
	int **T;
	M = new int*[n];
	for(int i = 0; i < n; i++)
		M[i] = new int[n];
	for(int i = 0; i < n; i++)
		for(int j = 0; j < n; j++)
			f >> M[i][j];
	f.close();

	cout << "Data read from file!";

	T = new int*[n];
	for(int i = 0; i < n; i++)
		T[i] = new int[n];

	for(int i = 0; i < n; i++)
		for(int j = 0; j < n; j++)
			T[i][j] = 0;

	//calculam T
	for(int i = n-1; i >= 0; i--)
		for(int j = n-1; j >= 0; j--)
			T[i][j] = M[i][j] + max((i == n-1 ? 0 : 1)*T[i+1][j], (j == n-1 ? 0 : 1) * T[i][j+1]);


	for(int i = 0; i < n; i++) {
		for(int j = 0; j < n; j++)
			cout << T[i][j] << ' ';
		cout << '\n';
	}
	return 0;
}
