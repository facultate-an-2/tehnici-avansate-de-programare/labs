#include <iostream>
#include <fstream>
#include <vector>
#include <limits>

using namespace std;

vector<int> readData() {
	vector<int> sizes;
	ifstream f("date.in");
	int n;
	f >> n;
	for(int i = 0; i < n; i++) {
		int x;
		f >> x;
		sizes.push_back(x);
	}
	f.close();
	return sizes;
}

void printMatrix(int **m, int dim) {
	for(int i = 0; i < dim; i++) {
		for(int j = 0; j < dim; j++) 
			cout << m[i][j] << ' ';
		cout << '\n';
	}
	cout << '\n';
}

void printVector(vector<int> vec) {
	for(int i = 0; i < vec.size(); i++)
		cout << vec[i] << ' ';
	cout << '\n';
}
/**
 * @brief Se da un sir de n matrici A1 A2 ... An de dimensiuni 
 * (p0, p1) (p1, p2) ... (pn - 1, pn). Ideea de rezolvare consta in rezolvarea problemelor pe subsiruri in ordinea crescatoarea a lungimii lor.
 * Fie in interval i, j. Trebuie sa gasesc k intre i si j a.i. nr de inmultiri sa fie optim de la i la k si de la k la j. 
 * Fie M[i][j] costul optim pt a inmulti Ai Ai+1 ... Ak
 * M[1][n] = ?
 * M[i][i] = 0
 * M[i][i+1] = pi-1 * pi * pi+1
 * M[i][i+len] = min { M[i][k] + M[k+1][i+len] + pi-1 * pk * pi }, i <= k < i + len - 1
 * pi-1 * pk * pi este dat de costul inmultirii celor doua matrici obtinute din subsirurile i..k si k+1 j
 * Input: n si p1 p2 ... pn
 * nr matrici = n-1
 * @param argc
 * @param argv
 * @return 
 */
int main(int argc, char **argv)
{
	int INF = numeric_limits<int>::max();
	
	vector<int> dimensions = readData();
	printVector(dimensions);
	int n = dimensions.size() - 1;
	int **m = new int*[n];
	
	for(int i = 0; i < n; i++)
		m[i] = new int[n]{ 0 };
	
	printMatrix(m, n);
	
	for(int i = 0; i < n; i++) {
		m[i][i] = 0;
		if(i < n - 1)
			m[i][i+1] = dimensions[i] * dimensions[i+1] * dimensions[i+2];
	}
	
	printMatrix(m, n);
	
	for(int len = 2; len < n; len++)
		for(int i = 0; i + len < n; i++) {
			int min = INF;
			for(int k = i; k < i + len; k++) {
				int currentSol = m[i][k] + m[k+1][i+len] + dimensions[i]*dimensions[k+1]*dimensions[i+len+1];
				cout << "\ni:" << i << ' ' << i+len << ' ' << currentSol << ' ';
				min = (currentSol < min) ? currentSol : min;
			} 
			m[i][i+len] = min;
		}
	cout << '\n';
	printMatrix(m, n);
	return 0;
}
