#include <iostream>
#include <fstream>
#include <vector>
#define INF (unsigned)!((int)0)
using namespace std;

/**
 * @brief Se da un vector cu elemente intregi si doi jucatori. Stiind ca fiecare jucator are voie sa aleaga
 * primul sau ultimul element din vectorul curent. Consideram o matrice M de n x n, unde m[i][j] este punctajul maxim
 * garantat avand vectorul de joc de la i pana la j 
 * @return 
 */

vector<int> readInitialArray() {
	ifstream f("date.in");
	int n;
	f >> n;
	vector<int> array;
	for(int i = 0; i < n; i++) {
		int x;
		f >> x;
		array.push_back(x);
	}
	f.close();
	return array;
}

/**
 * @brief Calculeaza M[i][j], indiferent de lungime. 
 * if len > 2 M[i][j] = max (
 * 								S[i] + min(M[i+1][j-1], M[i+2][j]_
 * 								S[j] + min(M[i+1][j-1], M[i][j-2]
 * 							)
 * @param matrix
 * @param intervalStart
 * @param intervalEnd
 * @param arr
 * @return 
 */
int getMaxGuaranteedForInterval(int **matrix, int intervalStart, int intervalEnd, vector<int>& arr) {
	if(intervalStart >= arr.size() || intervalStart < 0 || intervalEnd >= arr.size() || intervalEnd < 0 || intervalEnd < intervalStart)
		return INF;
	//daca avem un subvector de lungime 1 atunci scorul maxim garantat e clar acel element 
	if(intervalStart == intervalEnd)
		return arr[intervalStart];
	//daca subvectorul are lungime 2 atunci o sa fie maximul dintre cele 2 elemente
	if(intervalEnd - intervalStart == 1)
		return max(arr[intervalStart], arr[intervalEnd]);
	
	return max(arr[intervalStart] + min(matrix[intervalStart + 1][intervalEnd - 1], matrix[intervalStart + 2][intervalEnd]), 
			   arr[intervalEnd] + min(matrix[intervalStart + 1][intervalEnd - 1], matrix[intervalStart][intervalEnd - 2]));
}

void printSquareMatrix(int **matrix, int dim) {
	cout << '\n';
	for(int i = 0; i < dim; i++) {
		for(int j = 0; j < dim; j++)
			cout << matrix[i][j] << ' ';
		cout << '\n';
	}
}

int main(int argc, char **argv)
{
	vector<int> arr = readInitialArray();
	int n = arr.size();
	int **firstPlayerMaxGuaranteed = new int*[n];
	for(int i = 0; i < arr.size(); i++)
		firstPlayerMaxGuaranteed[i] = new int[n]{0};
	
	for(int i = 0; i < n; i++)
		firstPlayerMaxGuaranteed[i][i] = getMaxGuaranteedForInterval(firstPlayerMaxGuaranteed, i, i, arr);
	for(int i = 0; i < n - 1; i++)
		firstPlayerMaxGuaranteed[i][i + 1] = getMaxGuaranteedForInterval(firstPlayerMaxGuaranteed, i, i + 1, arr);
	
//	for(int i = 0; i < n; i++)
//		for(int j = i; j < n; j++)
//			if(!firstPlayerMaxGuaranteed[i][j])
//				firstPlayerMaxGuaranteed[i][j] = getMaxGuaranteedForInterval(firstPlayerMaxGuaranteed, i, j, arr); 
	for(int len = 2; len < n; len++)
		for(int i = 0; i + len < n; i++)
			firstPlayerMaxGuaranteed[i][i + len] = getMaxGuaranteedForInterval(firstPlayerMaxGuaranteed, i, i + len, arr);
		
	printSquareMatrix(firstPlayerMaxGuaranteed, n);
	
	//simulate the game
	int intervalStart = 0, intervalEnd = n - 1;
	vector<int> playerArr;
	vector<int> computerArr;
	
	while(intervalStart <= intervalEnd) {
		//the computer chooses first
		//TODO : SIMULARE JOC
	}
	return 0;
}
